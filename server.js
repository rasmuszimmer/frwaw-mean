/**
 * Created by RasmusZimmer on 06-04-2015.
 */
var express  = require('express');
var app      = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;

// configuration =================

//------- MongoDB
mongoose.connect('mongodb://localhost/MEAN2');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log('Connected to DB');
});

// User Schema
var userSchema = mongoose.Schema({
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true}
});

// Bcrypt middleware
userSchema.pre('save', function(next) {
    var user = this;

    if(!user.isModified('password')) return next();

    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if(err) return next(err);

        bcrypt.hash(user.password, salt, function(err, hash) {
            if(err) return next(err);
            user.password = hash;
            next();
        });
    });
});

// Password verification
userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if(err) return cb(err);
        cb(null, isMatch);
    });
};

var User = mongoose.model('User', userSchema);
User.remove({}, function(err) {
    console.log('collection removed');
});

var user = new User({ username: 'bob', email: 'bob@example.com', password: 'secret' });
user.save(function(err) {
    if(err) {
        console.log(err);
    } else {
        console.log('user: ' + user.username + " saved.");
    }
});

//------------------------------------------- EVENTS SETUP ----------------------------------------------
var Event = mongoose.model('Event', {
    title : String,
    content: String,
    isCandidate: Boolean,
    date: Date,
    venue: String,
    imdbID: String,
    fblink: String
});

// Events API
app.get('/events', function(req, res) {

    Event.find(function(err, events) {

        if (err)
            res.send(err);

        //res.send("events API works!");
        res.json(["dwadwa", "aadaf"]);
    });
});

app.post('/events', function(req, res) {

    Event.create({
        title : req.body.title,
        content: req.body.content,
        isCandidate: req.body.isCandidate,
        date: req.body.date,
        venue: req.body.venue,
        imdbID: req.body.imdbID,
        fblink: req.body.fblink
    }, function(err, event) {
        if (err)
            res.send(err);

        Event.find(function(err, events) {
            if (err)
                res.send(err)
            res.json(events);
        });
    });

});

app.delete('/events/:id', function(req, res) {
    Event.remove({
        _id : req.params.id
    }, function(err, event) {
        if (err)
            res.send(err);

        Event.find(function(err, events) {
            if (err)
                res.send(err)
            res.json(events);
        });
    });
});

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.
passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

// Use the LocalStrategy within Passport.
//   Strategies in passport require a `verify` function, which accept
//   credentials (in this case, a username and password), and invoke a callback
//   with a user object.  In the real world, this would query a database;
//   however, in this example we are using a baked-in set of users.
passport.use(new LocalStrategy(function(username, password, done) {
    User.findOne({ username: username }, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false, { message: 'Unknown user ' + username }); }
        user.comparePassword(password, function(err, isMatch) {
            if (err) return done(err);
            if(isMatch) {
                return done(null, user);
            } else {
                return done(null, false, { message: 'Invalid password' });
            }
        });
    });
}));

app.use(express.static('public'));                 // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

app.use(session({
    genid: function(req) {
        return generateUUID(); // use UUIDs for session IDs
    },
    secret: 'keyboard cat'
}));
app.use(passport.initialize());
app.use(passport.session());

app.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err) { return next(err) }
        if (!user) {
            req.session.messages =  [info.message];
            return res.redirect('/login');
        }
        req.logIn(user, function(err) {
            if (err) { return next(err); }
            return res.redirect('/');
        });
    })(req, res, next);
});

app.get('/login', function(req, res) {
    res.sendfile('login.html');
});

app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

app.get('/admin', ensureAuthenticated, function(req, res) {
    res.sendfile('admin.html');
});

app.get('/', ensureAuthenticated, function(req, res) {
    res.sendfile('index.html');
});


// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");

// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.redirect('/login')
}

function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};