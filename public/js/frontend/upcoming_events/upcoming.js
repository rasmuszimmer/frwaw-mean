/**
 * Created by RasmusZimmer on 07-04-2015.
 */
'use strict';
define([
    'angular',
    'angularRoute'
], function(angular) {
    angular.module('itufilm.upcoming', ['ngRoute'])
        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/upcoming', {
                templateUrl: 'js/frontend/upcoming_events/template.html',
                controller: 'upcomingCtrl'
            });
        }])
        // We can load the controller only when needed from an external file
        .controller('upcomingCtrl', ['$scope', '$injector', function($scope, $injector) {
            require(['upcoming_events/upcomingCtrl'], function(ctrl) {
                // injector method takes an array of modules as the first argument
                // if you want your controller to be able to use components from
                // any of your other modules, make sure you include it together with 'ng'
                // Furthermore we need to pass on the $scope as it's unique to this controller
                $injector.invoke(ctrl, this, {'$scope': $scope});
            });
        }]);
});