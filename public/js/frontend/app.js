/**
 * Created by RasmusZimmer on 06-04-2015.
 */
'use strict';

define([
    'angular',
    'angularRoute',
    'controllers',
    'upcoming_events/upcoming',
    'next_showing/next'
], function(angular, angularRoute, controllers, upcoming, next) {
    // Declare app level module which depends on views, and components
    return angular.module('itufilm', [
        'ngRoute',
        'itufilm.upcoming',
        'itufilm.next'
    ]).
        config(['$routeProvider', function($routeProvider) {
            //$routeProvider.otherwise({redirectTo: '/upcoming'});
        }])
        .controller('NavbarController', controllers.navBarCtrl());
});