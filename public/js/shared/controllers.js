/**
 * Created by RasmusZimmer on 07-04-2015.
 */
define([], function() {
    return new function() {

        this.navBarCtrl = function () {
            return ['$scope', '$location', function($scope, $location) {

                $scope.isActive = function (viewLocation) {
                    return viewLocation === $location.path();
                };
            }]
        };
    };
});
