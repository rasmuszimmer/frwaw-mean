/**
 * Created by RasmusZimmer on 07-04-2015.
 */
'use strict';

define([], function() {
    return ['$scope', 'eventsProvider', function($scope, eventsProvider) {
        // You can access the scope of the controller from here
        $scope.welcomeMessage = 'hey this is eventsCtrl.js!';
        $scope.events = [];

        eventsProvider.$getEvents(function(data) {
            $scope.events = data;
        });





        // because this has happened asynchroneusly we've missed
        // Angular's initial call to $apply after the controller has been loaded
        // hence we need to explicityly call it at the end of our Controller constructor
        $scope.$apply();
    }];
});