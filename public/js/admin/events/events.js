/**
 * Created by RasmusZimmer on 07-04-2015.
 */
'use strict';
define([
    'angular',
    'angularRoute'
], function(angular) {
    angular.module('itufilmAdmin.events', ['ngRoute'])
        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/events', {
                templateUrl: 'js/admin/events/template.html',
                controller: 'eventsCtrl'
            });
        }])
        // We can load the controller only when needed from an external file
        .controller('eventsCtrl', ['$scope', '$injector', 'eventsProvider', function($scope, $injector, eventsProvider) {
            require(['events/eventsCtrl'], function(ctrl) {
                // injector method takes an array of modules as the first argument
                // if you want your controller to be able to use components from
                // any of your other modules, make sure you include it together with 'ng'
                // Furthermore we need to pass on the $scope as it's unique to this controller
                $injector.invoke(ctrl, this, {'$scope': $scope, eventsProvider: eventsProvider});
            });
        }])
        .factory('eventsProvider', ['$http', function($http) {

            function getEvents(cb) {
                $http.get('/events').
                    success(function(data, status, headers, config) {
                        // this callback will be called asynchronously
                        // when the response is available
                        cb(data);
                    }).
                    error(function(data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
            }

            return {
                $getEvents: getEvents
            };
        }]);
});