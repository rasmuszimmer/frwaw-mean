/**
 * Created by RasmusZimmer on 07-04-2015.
 */
'use strict';
require.config({
    paths: {
        angular: '../bower_components/angular/angular',
        angularRoute: '../bower_components/angular-route/angular-route',
        sharedControllers: '../shared/controllers'
    },
    shim: {
        'angular' : {'exports' : 'angular'},
        'angularRoute': ['angular']
    },
    priority: [
        "angular"
    ]
});

require([
        'angular',
        'admin'
    ], function(angular, app) {
        var $html = angular.element(document.getElementsByTagName('html')[0]);
        angular.element().ready(function() {
            // bootstrap the app manually
            angular.bootstrap(document, ['itufilmAdmin']);
        });
    }
);