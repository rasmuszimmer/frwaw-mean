/**
 * Created by RasmusZimmer on 06-04-2015.
 */
'use strict';

define([
    'angular',
    'angularRoute',
    'sharedControllers',
    'events/events',
    //'next_showing/next'
], function(angular, angularRoute, sharedControllers, events) {
    // Declare app level module which depends on views, and components
    return angular.module('itufilmAdmin', [
        'ngRoute',
        'itufilmAdmin.events'
        //'itufilm.next'
    ]).
        config(['$routeProvider', function($routeProvider) {
            //$routeProvider.otherwise({redirectTo: '/upcoming'});
        }])
        .controller('NavbarController', sharedControllers.navBarCtrl());
});